import React, { useState, useEffect } from 'react';
import Layout from '../components/layout';
import GroupList from '../containers/GroupList/GroupList';
import MessagesList from '../containers/MessagesList/MessagesList';

import PopUp from '../components/PopUp/PopUp';
import GroupCreate from '../containers/GroupCreate/GroupCreate';
import ToastNotification from '../components/ToastNotification/ToastNotification';

import CurrentGroupInfo from '../components/CurrentGroupInfo/CurrentGroupInfo';

import Pusher from 'pusher-js';
import axios from 'axios';
import * as dayjs from 'dayjs';

const Conversations = ({ query }) => {
  const [groups, setGroups] = useState([]);
  const [open, setOpen] = useState(false);
  const [errors, setErrors] = useState('');
  const [current, setCurrent] = useState('');
  const [subscribedChannels, setSubscribedChannels] = useState([]);
  const [subscribedCurrentChannel, setSubscribedCurrentChannel] = useState(
    null
  );

  const pusher = new Pusher(process.env.PUSHER_ID, {
    cluster: 'eu',
  });

  useEffect(() => {
    const config = {
      method: 'get',
      url: `${process.env.API_ENDPOINT}/user_groups`,
      headers: {
        token: localStorage.getItem('token'),
        id: localStorage.getItem('id'),
      },
    };

    axios(config)
      .then((res) => {
        if (query.id) {
          setCurrent(query.id);
        } else if (window.innerWidth > 991) {
          updateOrder(res.data);
          setCurrent(res.data[0].id);
        }

        setGroups(res.data);

        setSubscribedChannels(
          res.data.map((group) => pusher.subscribe(`group_${group.id}`))
        );
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      groups.map((group) => pusher.unsubscribe(`group_${group.id}`));
    };
  }, []);

  useEffect(() => {
    setSubscribedCurrentChannel(
      subscribedChannels.find((ch) => ch.name === `group_${current}`)
    );
  }, [subscribedChannels]);

  const updateOrder = (group) => {
    group.sort((a, b) => {
      if (!a.contents.length && !b.contents.length) return 0;
      if (a.contents.length && !b.contents.length) return -1;
      if (!a.contents.length && b.contents.length) return 1;

      return -dayjs(a.contents[0].createdAt).diff(
        dayjs(b.contents[0].createdAt)
      );
    });
    return group;
  };

  return (
    <Layout active={'conversations'} small={true} className='flex'>
      <GroupList
        groups={groups}
        setOpen={setOpen}
        current={current}
        setCurrent={setCurrent}
        subscribedChannels={subscribedChannels}
      />

      {groups.length && (
        <>
          <MessagesList
            currentId={current}
            setCurrentId={setCurrent}
            channel={subscribedCurrentChannel}
          />

          <CurrentGroupInfo
            currentId={current}
            currentGroup={groups.find((group) => group.id === current)}
          />
        </>
      )}

      <PopUp isOpen={open} onClick={() => setOpen(false)}>
        <GroupCreate setErrors={setErrors} />
      </PopUp>
      {errors && <ToastNotification msg={errors} />}
    </Layout>
  );
};

Conversations.getInitialProps = ({ query }) => {
  return { query };
};

export default Conversations;
