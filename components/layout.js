import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import styles from './layout.module.scss';

import Navbar from './Navbar/Navbar';

const layout = ({
  small = false,
  active,
  topBar = false,
  children,
  className = '',
}) => {
  const router = useRouter();

  const menu = [
    {
      label: 'Conversations',
      slug: 'conversations',
      icon: 'msg',
    },
    {
      label: 'Accueil',
      slug: 'groupe',
      icon: 'home',
    },
    {
      label: 'Évenements',
      slug: 'evenements',
      icon: 'calendar',
    },
  ];

  useEffect(() => {
    if (!localStorage.getItem('firstname')) {
      router.replace('/');
    }
  }, []);

  return (
    <div>
      <Navbar items={menu} active={active} small={small} />
      <div
        className={`${styles.content} ${small && styles.small} ${
          topBar && styles.topBar
        } ${className}`}
      >
        {children}
      </div>
    </div>
  );
};

export default layout;
