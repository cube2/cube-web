import React from 'react';
import EventList from '../containers/EventList/EventList';
import Layout from '../components/layout';

const Evenements = () => {
  return (
    <Layout active={'evenements'} className='flex'>
      <div className='w-100'>
        <EventList />
      </div>
    </Layout>
  );
};

export default Evenements;
