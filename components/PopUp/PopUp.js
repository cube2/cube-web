import React, { useEffect } from 'react';
import styles from './PopUp.module.scss';
import Sheet from 'react-modal-sheet';

const PopUp = ({ isOpen, children, onClick }) => {
  return (
    <Sheet isOpen={isOpen} onClose={onClick}>
      <Sheet.Container>
        <Sheet.Header />
        <Sheet.Content>{children}</Sheet.Content>
      </Sheet.Container>

      <Sheet.Backdrop />
    </Sheet>
  );
};

export default PopUp;
