import React from 'react';
import styles from './Profile.module.scss';

const profile = ({ url, name, long = false }) => {
  return (
    <div className={`${styles.profile} ${long ? styles.profile__long : ''}`}>
      <img src={url} alt='' />
      <p>{name}</p>
    </div>
  );
};

export default profile;
