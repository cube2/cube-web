import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import SignInWith from '../containers/SignInWith/SignInWith';
import HeadTitle from '../containers/HeadTitle/HeadTitle';
import StepForm from '../containers/StepForm/StepForm';
import ToastNotification from '../components/ToastNotification/ToastNotification';

const inscription = () => {
  const [step, setStep] = useState(1);
  const [title, setTitle] = useState('Bonjour,');
  const [subtitle, setSubtitle] = useState('Présente toi !');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [errors, setErrors] = useState(null);

  const router = useRouter();

  useEffect(() => {
    if (localStorage.getItem('firstname')) {
      router.replace('/groupe');
    }
  }, []);

  useEffect(() => {
    switch (step) {
      case 1:
        setSubtitle('Présente toi !');
        break;
      case 2:
        setSubtitle(name);
        break;
      default:
        break;
    }
  }, [step]);

  return (
    <div className='container'>
      <div className='d-flex flex-column vh-100 justify-content-center'>
        {step < 4 && <HeadTitle title={title} subtitle={subtitle} />}
        <StepForm
          step={step}
          setStep={setStep}
          name={name}
          setName={setName}
          email={email}
          setEmail={setEmail}
          phone={phone}
          setPhone={setPhone}
          errors={errors}
          setErrors={setErrors}
        />
        {step <= 1 && (
          <>
            <SignInWith />
            <p className='text-center m-0 mt-5 white-50'>
              Vous avez déja un compte ?
              <Link href='/connexion'>
                <a className='white font-weight-bold sm-font-size'>
                  {' '}
                  Connexion
                </a>
              </Link>
            </p>
          </>
        )}
      </div>
      {errors !== null && <ToastNotification msg={errors} />}
    </div>
  );
};

export default inscription;
