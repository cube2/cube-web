import React, { useState, useEffect } from 'react';
import Layout from '../components/layout';
import EventMessagesList from '../containers/EventMessagesList/EventMessagesList';
import CurrentEventInfo from '../components/CurrentEventInfo/CurrentEventInfo';

const Evenement = ({ query }) => {
  return (
    <Layout active={'conversations'} className='flex'>
      <EventMessagesList currentId={query.eventId} />
      <CurrentEventInfo currentId={query.eventId} />
    </Layout>
  );
};

Evenement.getInitialProps = ({ query }) => {
  return { query };
};

export default Evenement;
