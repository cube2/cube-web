import React, { useEffect } from 'react';
import styles from './SingleGroup.module.scss';

const SingleGroup = ({
  picture,
  groupName,
  isPublic,
  lastMessage,
  notif,
  current,
  onClick,
}) => {
  const displayHours = (date) => {
    const DATE = new Date(date);
    const hours = DATE.getHours();
    const minutes =
      DATE.getMinutes() < 10 ? '0' + DATE.getMinutes() : DATE.getMinutes();

    return `${hours}h${minutes}`;
  };

  return (
    <div
      className={`${styles.singleGroup} ${
        current && styles.singleGroupCurrent
      }`}
      onClick={onClick}
    >
      <div className={styles.singleGroup__image}>
        <img src={picture} alt='' />
      </div>
      <div className={styles.singleGroup__content}>
        <div className={styles.singleGroup__content__name}>
          <p>{groupName}</p>
          {!isPublic && <img src='/img/svg/icn-lock.svg' alt='' />}
        </div>
        {lastMessage && (
          <div className={styles.singleGroup__content__last}>
            <img src={lastMessage.author.picture} alt='' />
            <p>
              {lastMessage.type === 'image'
                ? `${lastMessage.author.firstname} à envoyé une photo`
                : lastMessage.value}
            </p>
          </div>
        )}
      </div>
      {lastMessage && (
        <div className={styles.singleGroup__date}>
          <div>
            <span>{1}</span>
          </div>
          <p>{displayHours(lastMessage.createdAt)}</p>
        </div>
      )}
    </div>
  );
};

export default SingleGroup;
