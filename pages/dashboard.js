import React from 'react';
import DashboardPanel from '../containers/DashboardPanel/DashboardPanel';

const Dashboard = () => {
  return <DashboardPanel />;
};

export default Dashboard;
