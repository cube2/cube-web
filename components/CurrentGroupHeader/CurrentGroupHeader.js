import React, { useState, useEffect } from 'react';
import styles from './CurrentGroupHeader.module.scss';

import PopUp from '../PopUp/PopUp';
import SingleEvent from '../SingleEvent/SingleEvent';
import EventList from '../EventList/EventList';

const CurrentGroupHeader = ({ setCurrentId, currentGroup }) => {
  const [open, setOpen] = useState(false);
  const [openGroupInfo, setOpenGroupInfo] = useState(false);
  const [errors, setErrors] = useState('');

  useEffect(() => {
    console.log(currentGroup.events, 'cur');
  }, []);
  return (
    <>
      <div className={styles.CurrentGroupHeader}>
        <img
          src='/img/svg/icn-back.svg'
          alt=''
          onClick={() => setCurrentId(null)}
        />
        <div className={styles.CurrentGroupHeader__info}>
          <img
            src={currentGroup.picture}
            alt=''
            onClick={() => setOpenGroupInfo(true)}
          />
          <div>
            <p>{currentGroup.name}</p>
            <span>{currentGroup.members.length} membres</span>
          </div>
        </div>
        <img
          src='/img/svg/icn-calendar.svg'
          alt=''
          onClick={() => setOpen(true)}
        />

        <div className={styles.CurrentGroupHeader__more}>
          <img
            src='/img/svg/icn-moreVertical.svg'
            alt=''
            onClick={() => setOpenGroupInfo(true)}
          />
        </div>
      </div>

      <PopUp isOpen={open} onClick={() => setOpen(false)}>
        <EventList events={currentGroup.events} />
      </PopUp>

      <PopUp isOpen={openGroupInfo} onClick={() => setOpenGroupInfo(false)}>
        group Info
      </PopUp>
    </>
  );
};

export default CurrentGroupHeader;
