import React from 'react';
import Link from 'next/link';

const Button = ({ children, href, className, type, onClick = null }) => {
  return href ? (
    <Link href={href}>
      <a className={`${className} px-4 py-3`}>{children}</a>
    </Link>
  ) : (
    <button type={type} className={`${className}`} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
