import React, { useState, useEffect } from 'react';
import styles from './GroupList.module.scss';
import LogoButton from '../../components/LogoButton/LogoButton';
import PrivateMessageList from '../../components/PrivateMessageList/PrivateMessageList';
import * as dayjs from 'dayjs';
import SingleGroup from '../../components/SingleGroup/SingleGroup';

const GroupList = ({
  groups,
  setOpen,
  current,
  setCurrent,
  subscribedChannels,
}) => {
  const [active, setActive] = useState('groupes');
  const [groupsState, setGroupsState] = useState([]);
  const [first, setFirst] = useState(true);

  const [privateMsg, setPrivateMsg] = useState([
    {
      name: 'Marie',
      picture:
        'https://i.picsum.photos/id/1011/5472/3648.jpg?hmac=Koo9845x2akkVzVFX3xxAc9BCkeGYA9VRVfLE4f0Zzk',
    },
    {
      name: 'Tom',
      picture:
        'https://i.picsum.photos/id/1011/5472/3648.jpg?hmac=Koo9845x2akkVzVFX3xxAc9BCkeGYA9VRVfLE4f0Zzk',
    },
    {
      name: 'Kevin',
      picture:
        'https://i.picsum.photos/id/1011/5472/3648.jpg?hmac=Koo9845x2akkVzVFX3xxAc9BCkeGYA9VRVfLE4f0Zzk',
    },
    {
      name: 'Marie',
      picture:
        'https://i.picsum.photos/id/1011/5472/3648.jpg?hmac=Koo9845x2akkVzVFX3xxAc9BCkeGYA9VRVfLE4f0Zzk',
    },
    {
      name: 'Tom',
      picture:
        'https://i.picsum.photos/id/1011/5472/3648.jpg?hmac=Koo9845x2akkVzVFX3xxAc9BCkeGYA9VRVfLE4f0Zzk',
    },
    {
      name: 'Kevin',
      picture:
        'https://i.picsum.photos/id/1011/5472/3648.jpg?hmac=Koo9845x2akkVzVFX3xxAc9BCkeGYA9VRVfLE4f0Zzk',
    },
  ]);

  useEffect(() => {
    console.log(groups, 'groups');
    subscribedChannels.map((channel) => {
      channel.bind('receiveMessage', (data) => {
        console.log(data, ' DATA REGARDEEEEEE');
        let updatedGroups = [...groupsState];
        updatedGroups.map((group) => {
          if (group.id === data.id_group) {
            console.log(group, ' Group REGARDEEEEEE');
            console.log(data.type, ' Type REGARDEEEEEE');
            group.contents[0].value =
              data.type === 'image'
                ? `${data.author.firstname} à envoyé une photo`
                : data.value;
            group.contents[0].createdAt = data.createdAt;
            group.contents[0].author = data.author;
          }
        });
        setGroupsState(updatedGroups);
      });
    });
  }, [subscribedChannels]);

  useEffect(() => {
    if (groups.length) {
      setGroupsState(groups);
    }
  }, [groups]);

  // useEffect(() => {
  //   if (groupsState.length && first) {
  //     setTimeout(() => {
  //       let updatedGroups = [...groupsState];

  //       updatedGroups.map((group) => {
  //         if (group.id === '604e89ba7a2cad50581b9e82') {
  //           group.contents[0].value = 'Nouveau';
  //           group.contents[0].createdAt = '2021-05-04 12:18:29';
  //         }
  //       });

  //       setFirst(false);
  //       setGroupsState(updatedGroups);
  //     }, 5000);
  //   }
  // }, [groupsState]);

  const updateOrder = () => {
    let orderedList = [...groupsState];
    orderedList.sort((a, b) => {
      if (!a.contents.length && !b.contents.length) return 0;
      if (a.contents.length && !b.contents.length) return -1;
      if (!a.contents.length && b.contents.length) return 1;

      return -dayjs(a.contents[0].createdAt).diff(
        dayjs(b.contents[0].createdAt)
      );
    });
    return orderedList;
  };

  return (
    <div className={styles.groupList}>
      <div className={styles.groupList__group}>
        <h2>Mes groupes</h2>

        <div className={styles.groupList__group__mobileOption}>
          <p>Mes groupes</p>
          <div className={styles.groupList__group__mobileOption__btn}>
            <LogoButton
              icon={'icn-search'}
              className={'noAbsolute'}
              color={'transparent'}
              size={'small'}
              onClick={() => {}}
            />
            <LogoButton
              icon={'icn-plus'}
              className={'noAbsolute'}
              size={'small'}
              onClick={() => setOpen(true)}
            />
          </div>
        </div>
        <div className={styles.groupList__group__add}>
          <LogoButton
            icon={'icn-plus'}
            size={'medium'}
            onClick={() => setOpen(true)}
          />
        </div>

        <div className={styles.groupList__group__list}>
          {!groups.length ? (
            <p className='text-center'>Vous n'avez aucun groupe</p>
          ) : (
            <div>
              {updateOrder().map((group, i) => {
                return (
                  <SingleGroup
                    key={i}
                    picture={group.picture}
                    groupName={group.name}
                    isPublic={group.isPublic}
                    lastMessage={group.contents[0]}
                    notif={group.notif}
                    current={group.id === current ? true : false}
                    onClick={() => setCurrent(group.id)}
                  />
                );
              })}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default GroupList;
