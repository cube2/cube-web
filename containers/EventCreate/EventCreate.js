import React, { useState } from 'react';
import styles from './EventCreate.module.scss';
import Button from '../../components/Button/Button';

import Compressor from 'compressorjs';
import DatePicker from '../../components/DatePicker/DatePicker';
import 'react-datepicker/dist/react-datepicker.css';

import axios from 'axios';

const EventCreate = ({ setErrors, currentId, close }) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [location, setLocation] = useState('');
  const [date, setDate] = useState(new Date());

  const [file, setFile] = useState(null);
  const [picture, setPicture] = useState('');

  const makeCompressor = (file, options) => {
    return new Compressor(file, options);
  };

  const handleSubmit = () => {
    if (name && description && date) {
      setErrors('');
      makeCompressor(picture, {
        quality: 0.6,
        width: 500,
        success(result) {
          const formData = new FormData();

          formData.append('file', result, result.name);
          formData.append('upload_preset', 'h19ajqlm');

          formData.append('folder', 'event_group');

          axios
            .post(
              'https://api.cloudinary.com/v1_1/theo-cesi/image/upload',
              formData
            )
            .then((res) => {
              console.log(res, 'cloudinary');

              //TODO: Event
              const config = {
                method: 'post',
                url: `${process.env.API_ENDPOINT}/create_event`,
                headers: {
                  token: localStorage.getItem('token'),
                  id: localStorage.getItem('id'),
                },
                data: {
                  id_group: currentId,
                  picture: res.data.url,
                  description: description,
                  name: name,
                  location: location,
                  date_event: date,
                },
              };

              axios(config)
                .then((res) => {
                  console.log(res, 'eventCreated');
                  if (res.data.status === 202) {
                    close();
                  }
                })
                .catch((error) => {
                  console.log(error);
                  setErrors(error);
                });
            });
        },
        error(err) {
          setErrors(err.message);
        },
      });
    } else {
      setErrors('Certains champs sont manquants');
    }
  };

  const handleChange = (event) => {
    setFile(URL.createObjectURL(event.target.files[0]));
    setPicture(event.target.files[0]);
  };

  return (
    <div className={styles.GroupCreate}>
      <label className={styles.GroupCreate__pickImage}>
        <input type='file' onChange={handleChange} />
        <img src={file} border='0' />
      </label>
      <div className={styles.GroupCreate__inputOffset}>
        <label>Nom de l'évenement</label>
        <input
          type='text'
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      <div className={styles.GroupCreate__inputOffset}>
        <label>Description</label>
        <textarea
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        ></textarea>
      </div>

      <div className={styles.GroupCreate__datePicker}>
        <label>Date</label>
        <DatePicker setDate={setDate} />
      </div>

      <div className={styles.GroupCreate__inputOffset}>
        <label>Lieu</label>
        <input
          type='text'
          value={location}
          onChange={(e) => setLocation(e.target.value)}
        />
      </div>

      <Button
        type='submit'
        className='white btn-submit py-2 px-4 mt-3'
        onClick={handleSubmit}
      >
        Suivant
      </Button>
    </div>
  );
};

export default EventCreate;
