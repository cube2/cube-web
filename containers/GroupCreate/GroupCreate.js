import React, { useState, useEffect } from 'react';
import styles from './GroupCreate.module.scss';
import Button from '../../components/Button/Button';

import axios from 'axios';
import Radio from '../../components/Radio/Radio';
import Compressor from 'compressorjs';

import { useRouter } from 'next/router';

const GroupCreate = ({ setErrors }) => {
  const router = useRouter();
  const [type, setType] = useState('public');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [currentStep, setCurrentStep] = useState(1);
  const [groupId, setGroupId] = useState('');

  const [file, setFile] = useState(null);
  const [picture, setPicture] = useState('');
  const [search, setSearch] = useState('');
  const [initialUsers, setInitialUsers] = useState([]);
  const [users, setUsers] = useState([]);
  const [membersToAdd, setMembersToAdd] = useState([]);

  const makeCompressor = (file, options, close) => {
    return new Compressor(file, options);
  };

  const inviteMembers = (user) => {
    let members = [...membersToAdd];

    if (members.indexOf(user.id) === -1) {
      members.push(user.id);
    } else {
      members.splice(members.indexOf(user.id), 1);
    }

    setMembersToAdd(members);
  };

  const adddMember = () => {
    const config = {
      method: 'post',
      url: `${process.env.API_ENDPOINT}/add_members`,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      data: {
        id: groupId,
        user_ids: membersToAdd,
      },
    };

    axios(config).then((res) => {
      console.log(res, 'gooooooo');
      router.push(`/conversations?id=${groupId}`);
    });
  };

  const handleSubmit = () => {
    if (name && description && type) {
      setErrors('');
      makeCompressor(picture, {
        quality: 0.6,
        width: 500,
        success(result) {
          const formData = new FormData();
          formData.append('file', result, result.name);
          formData.append('upload_preset', 'h19ajqlm');
          formData.append('folder', 'picture_group');

          axios
            .post(
              'https://api.cloudinary.com/v1_1/theo-cesi/image/upload',
              formData
            )
            .then((res) => {
              const config = {
                method: 'post',
                url: `${process.env.API_ENDPOINT}/created_group`,
                headers: {
                  Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
                data: {
                  name: name,
                  description: description,
                  picture: res.data.url,
                  theme: 'Test',
                  isPublic: type === 'public' ? true : false,
                },
              };

              axios(config)
                .then((res) => {
                  console.log(res, 'post group');
                  if (res.status === 201) {
                    setGroupId(res.data.id_group);
                    setCurrentStep((current) => current + 1);
                  }
                })
                .catch((error) => {
                  setErrors(error);
                });
            });
        },
        error(err) {
          setErrors(err.message);
        },
      });
    } else {
      setErrors('Nom ou description manquante');
    }
  };

  const handleChange = (event) => {
    setFile(URL.createObjectURL(event.target.files[0]));
    setPicture(event.target.files[0]);
  };

  const searchMembers = () => {
    if (search === '') {
      setUsers(initialUsers);
    } else {
      const searched = initialUsers.filter((user) =>
        user.firstname.toLowerCase().includes(search.toLowerCase())
      );
      setUsers(searched);
    }
  };

  useEffect(() => {
    searchMembers();
  }, [search]);

  useEffect(() => {
    const config = {
      method: 'get',
      url: `${process.env.API_ENDPOINT}/get_active_users`,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };

    axios(config)
      .then((res) => {
        setInitialUsers(res.data);
      })
      .catch((error) => {
        setErrors(error);
      });
  }, []);

  useEffect(() => {
    if (initialUsers.length) {
      setUsers(initialUsers);
    }
  }, [initialUsers]);

  return (
    <>
      <GroupSettings
        currentStep={currentStep}
        file={file}
        name={name}
        setName={setName}
        description={description}
        setDescription={setDescription}
        type={type}
        setType={setType}
        handleChange={handleChange}
        handleSubmit={handleSubmit}
      />

      <AddMembers
        currentStep={currentStep}
        file={file}
        users={users}
        inviteMembers={inviteMembers}
        search={search}
        setSearch={setSearch}
        handleSubmit={adddMember}
      />
    </>
  );
};

export default GroupCreate;

function GroupSettings({
  currentStep,
  file,
  name,
  setName,
  description,
  setDescription,
  type,
  setType,
  handleChange,
  handleSubmit,
}) {
  if (currentStep !== 1) {
    return null;
  }
  return (
    <div className={styles.GroupCreate}>
      <label className={styles.GroupCreate__pickImage}>
        <input type='file' onChange={handleChange} />
        <img src={file} border='0' />
      </label>
      <div className={styles.GroupCreate__inputOffset}>
        <label>Nom du groupe</label>
        <input
          type='text'
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      <div className={styles.GroupCreate__inputOffset}>
        <label>Description</label>
        <textarea
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        ></textarea>
      </div>
      <div className={styles.GroupCreate__choseType}>
        <label>Type</label>
        <div className={styles.GroupCreate__choseType__public}>
          <div className={styles.GroupCreate__choseType__public__box}>
            <img src='/img/svg/icn-globe.svg' alt='' />
          </div>
          <div
            className={styles.GroupCreate__choseType__public__content}
            onClick={() => setType('public')}
          >
            <p>Public</p>
            <p>Tout le monde peut voir et rejoindre ce groupe</p>
          </div>
          <Radio value='public' selected={type} onChange={setType} />
        </div>
        <div className={styles.GroupCreate__choseType__private}>
          <div className={styles.GroupCreate__choseType__private__box}>
            <img src='/img/svg/icn-lock.svg' alt='' />
          </div>
          <div
            className={styles.GroupCreate__choseType__private__content}
            onClick={() => setType('private')}
          >
            <p>Privé</p>
            <p>Seule les personnes invités peuvent voir ce groupe</p>
          </div>
          <Radio value='private' selected={type} onChange={setType} />
        </div>
      </div>

      <Button
        type='submit'
        className='white btn-submit py-2 px-4 mt-3'
        onClick={handleSubmit}
      >
        Suivant
      </Button>
    </div>
  );
}

function AddMembers({
  currentStep,
  file,
  users,
  inviteMembers,
  search,
  setSearch,
  handleSubmit,
}) {
  if (currentStep !== 2) {
    return null;
  }
  return (
    <div className={styles.AddMembers}>
      <div className={styles.AddMembers__picture}>
        <img src={file} />
      </div>
      <div className={styles.AddMembers__input}>
        <input
          type='text'
          placeholder='Recherchez vos amis'
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <img src='/img/svg/icn-search.svg' alt='' />
      </div>

      <div className={styles.AddMembers__list}>
        {users.map((user) => (
          <div className={styles.AddMembers__list__member} key={user.id}>
            <img src={user.picture} />
            <p>
              {user.firstname} {user.lastname}
            </p>
            <Radio
              value='on'
              selected={'on'}
              selfActiveOnClick={true}
              onChange={() => inviteMembers(user)}
            />
          </div>
        ))}
      </div>
      <Button
        type='submit'
        className='white btn-submit py-2 px-4 mt-3'
        onClick={handleSubmit}
      >
        Terminer
      </Button>
    </div>
  );
}
