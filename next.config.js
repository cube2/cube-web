const withSass = require('@zeit/next-sass');
const withImages = require('next-images');
const withLess = require('@zeit/next-less');
const withCSS = require('@zeit/next-css');

module.exports = withCSS(
  withLess(
    withImages(
      withSass({
        env: {
          ANY_ENV_KEY: 'ANY_ENV_VARIABLE',
        },
      })
    )
  )
);

module.exports = {
  env: {
    API_ENDPOINT:
      process.env.API_ENV === 'production'
        ? 'https://nasmaison30.freeboxos.fr/api'
        : 'https://azure-api.francecentral.cloudapp.azure.com/api',
  },
};
