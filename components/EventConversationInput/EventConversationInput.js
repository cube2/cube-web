import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import Compressor from 'compressorjs';

import styles from './EventConversationInput.module.scss';

const EventConversationInput = ({
  setConversation,
  currentId,
  userId,
  openEventModal,
}) => {
  const [showImageInput, setShowImageInput] = useState(false);
  const [message, setMessage] = useState('');
  const [picture, setPicture] = useState('');
  const [file, setFile] = useState('');
  const [isOpen, setIsOpen] = useState(false);
  const [icon, setIcon] = useState('mic');
  const [userFirstname, setUserFirstname] = useState('');
  const inputRef = useRef(null);

  useEffect(() => {
    if (message != '') {
      setIcon('send');
    } else {
      setIcon('mic');
    }
  }, [message]);

  useEffect(() => {
    setUserFirstname(localStorage.getItem('firstname'));
  }, []);

  const openModal = () => {
    setIsOpen((isOpen) => !isOpen);
  };

  const makeCompressor = (file, options) => {
    return new Compressor(file, options);
  };

  const sendMessage = () => {
    if (message === '' && file === '') return;

    setConversation((conversation) => [
      ...conversation,
      {
        id: Math.floor(Math.random() * 1000),
        id_group: currentId,
        value: file ? file : message,
        author: {
          id: userId,
          firstname: userFirstname,
          lastname: '',
          picture: localStorage.getItem('picture'),
        },
        createdAt: new Date(),
        type: file ? 'image' : 'text',
      },
    ]);

    if (file != '') {
      makeCompressor(picture, {
        quality: 0.6,
        width: 500,
        success(result) {
          const formData = new FormData();

          formData.append('file', result, result.name);
          formData.append('upload_preset', 'h19ajqlm');

          formData.append('folder', 'picture_message');

          axios
            .post(
              'https://api.cloudinary.com/v1_1/theo-cesi/image/upload',
              formData
            )
            .then((res) => {
              sendToAPI(res.data.secure_url, 'image');
              closeInputImage();
            });
        },
        error(err) {
          console.log(err);
        },
      });
    } else {
      sendToAPI(message, 'text');
      setMessage('');
    }
  };

  const sendToAPI = (value, type) => {
    console.log(currentId, 'hhuuguig');
    const config = {
      method: 'post',
      url: `${process.env.API_ENDPOINT}/send_content_event`,
      headers: {
        token: localStorage.getItem('token'),
        id: localStorage.getItem('id'),
      },
      data: {
        id_event: currentId,
        value: value,
        type: type,
      },
    };

    axios(config)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleChange = (e) => {
    setMessage(e.target.value);

    inputRef.current.style.height = '';
    inputRef.current.style.height = inputRef.current.scrollHeight + 'px';
  };

  const uploadImage = (e) => {
    setIsOpen(false);
    setFile(URL.createObjectURL(e.target.files[0]));
    setPicture(e.target.files[0]);
    setShowImageInput(true);
    setIcon('send');
  };

  const closeInputImage = () => {
    setFile('');
    setPicture('');
    setShowImageInput(false);
    setIcon('mic');
  };

  return (
    <div className={styles.Input}>
      <div className={styles.Input__btn} onClick={openModal}>
        <img src='/img/svg/icn-plus.svg' alt='' />
      </div>
      {!showImageInput ? (
        <div className={styles.Input__area}>
          <textarea
            ref={inputRef}
            placeholder='Aa'
            value={message}
            onChange={handleChange}
          ></textarea>
          <img src='/img/svg/icn-paperclip.svg' alt='' onClick={openModal} />
        </div>
      ) : (
        <div className={styles.Input__withImage}>
          <div className={styles.Input__withImage__input}>
            <div className={styles.Input__withImage__input__imgBox}>
              <img src={file} alt='' />

              <div
                className={styles.Input__withImage__input__imgBox__close}
                onClick={closeInputImage}
              >
                <img src='/img/svg/icn-plus.svg' alt='' />
              </div>
            </div>
          </div>
        </div>
      )}

      <img src={`/img/svg/icn-${icon}.svg`} alt='' onClick={sendMessage} />

      {isOpen && (
        <>
          <div className={styles.Input__overlay} onClick={openModal}></div>
          <div className={styles.Input__modal}>
            <label>
              <div className={styles.Input__modal__item}>
                <img src={`/img/svg/icn-img.svg`} alt='' />
                <input type='file' onChange={uploadImage} />
                <p>Photo</p>
              </div>
            </label>
          </div>
          <div className={styles.Input__modalClose} onClick={openModal}>
            <p>Annuler</p>
          </div>
        </>
      )}
    </div>
  );
};

export default EventConversationInput;
