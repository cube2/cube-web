import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import styles from './EventMessagesList.module.scss';
import CurrentEventHeader from '../../components/CurrentEventHeader/CurrentEventHeader';
import EventConversationFeed from '../../components/EventConversationFeed/EventConversationFeed';
import Spinner from '../../components/Spinner/Spinner';

const EventMessagesList = ({ currentId }) => {
  const [currentEvent, setCurrentEvent] = useState(null);
  const [loading, setLoading] = useState(true);

  const containerRef = useRef(null);

  useEffect(() => {
    if (currentId) {
      setLoading(true);
      const config = {
        method: 'get',
        url: `${process.env.API_ENDPOINT}/get_event/${currentId}`,
        headers: {
          token: localStorage.getItem('token'),
          id: localStorage.getItem('id'),
        },
      };

      axios(config).then((res) => {
        console.log(res, 'Regarde');
        setCurrentEvent(res.data);
      });
    }
  }, [currentId]);

  useEffect(() => {
    if (currentEvent) {
      setLoading(false);
    }
  }, [currentEvent]);

  return (
    <div className={styles.MessagesList} ref={containerRef}>
      {loading ? (
        <Spinner className={'fixed'} />
      ) : (
        <>
          <CurrentEventHeader currentEvent={currentEvent} />
          <EventConversationFeed
            currentEvent={currentEvent}
            // channel={channel}
          />
        </>
      )}
    </div>
  );
};

export default EventMessagesList;
