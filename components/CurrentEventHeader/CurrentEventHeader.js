import React, { useState, useEffect } from 'react';
import styles from './CurrentEventHeader.module.scss';
import { useRouter } from 'next/router';

const CurrentEventHeader = ({ currentEvent }) => {
  const router = useRouter();
  useEffect(() => {
    console.log(currentEvent, 'event cur');
  }, []);

  const goToGroup = () => {
    router.push(`/conversations?id=${currentEvent.id_group}`);
  };
  return (
    <>
      <div className={styles.CurrentGroupHeader}>
        <img src='/img/svg/icn-back.svg' alt='' onClick={() => goToGroup()} />
        <div className={styles.CurrentGroupHeader__info}>
          <img src={currentEvent.picture} alt='' />
          <div>
            <p>{currentEvent.name}</p>
            <span>{currentEvent.participants.length + 1} membres </span>
          </div>
        </div>
        <div className={styles.CurrentGroupHeader__more}>
          <img src='/img/svg/icn-back.svg' alt='' onClick={() => goToGroup()} />
        </div>
      </div>
    </>
  );
};

export default CurrentEventHeader;
