import React, { useEffect } from 'react';
import styles from './ConversationMessage.module.scss';

const ConversationMessage = ({ message, userId }) => {
  const displayHours = (date) => {
    const DATE = new Date(date);
    const hours = DATE.getHours();
    const minutes =
      DATE.getMinutes() < 10 ? '0' + DATE.getMinutes() : DATE.getMinutes();

    return `${hours}h${minutes}`;
  };

  if (message.author.id === userId) {
    return (
      <div className={styles.myMessage} key={message.id}>
        {message.type === 'text' && (
          <div className={styles.myMessage__content}>{message.value}</div>
        )}
        {message.type === 'image' && (
          <div
            className={`${styles.myMessage__content} ${styles.myMessage__contentImage}`}
          >
            <img src={message.value} />
          </div>
        )}
        <span>{displayHours(message.createdAt)}</span>
      </div>
    );
  } else {
    return (
      <div className={styles.otherMessage} key={message.id}>
        <div className={styles.otherMessage__header}>
          <img src={message.author.picture} alt='' />
          <p>{message.author.firstname}</p>
          <span>{displayHours(message.createdAt)}</span>
        </div>

        {message.type === 'text' && (
          <div className={styles.otherMessage__content}>{message.value}</div>
        )}

        {message.type === 'image' && (
          <div
            className={`${styles.otherMessage__content} ${styles.otherMessage__contentImage}`}
          >
            <img src={message.value} />
          </div>
        )}
      </div>
    );
  }
};

export default ConversationMessage;
