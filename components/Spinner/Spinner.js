import React from 'react';
import styles from './Spinner.module.scss';

const Spinner = ({ className = '' }) => {
  return (
    <div className={`${styles.Spinner} ${className}`}>
      <div className={styles.Spinner__circle}></div>
    </div>
  );
};

export default Spinner;
