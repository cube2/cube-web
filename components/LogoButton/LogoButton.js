import React from 'react';
import styles from './LogoButton.module.scss';

const LogoButton = ({
  icon,
  className = '',
  onClick = null,
  color = 'green',
  size = '',
}) => {
  return (
    <div
      className={`${styles.logoButton} logoButton-${color} ${className} ${
        size === 'small' && styles.small
      } ${size === 'medium' && styles.medium}`}
      onClick={onClick}
    >
      <img src={`/img/svg/${icon}.svg`} alt='' />
    </div>
  );
};

export default LogoButton;
