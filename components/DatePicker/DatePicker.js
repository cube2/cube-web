import React, { useState } from 'react';
import DatePicker, { registerLocale } from 'react-datepicker';
import fr from 'date-fns/locale/fr'; // the locale you want
registerLocale('fr', fr); // register it with the name you want

const Datepicker = ({ setDate }) => {
  const [startDate, setStartDate] = useState(new Date());
  const [selectedDate, setSelectedDate] = useState(new Date());

  return (
    <DatePicker
      locale='fr'
      selected={selectedDate}
      closeOnScroll={(e) => e.target === document}
      onChange={(date) => {
        setDate(date);
        setSelectedDate(date);
      }}
      minDate={startDate}
    />
  );
};

export default Datepicker;
