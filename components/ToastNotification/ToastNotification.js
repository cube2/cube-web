import React, { useState, useEffect } from 'react';
import styles from './ToastNotification.module.scss';

const ToastNotification = ({ status = 'error', msg }) => {
  const [isOpen, setIsOpen] = useState(false);
  useEffect(() => {
    if (msg != null) {
      setIsOpen(true);
    }
  }, [msg]);

  return (
    <>
      {isOpen && (
        <div
          className={`${styles.toastNotification} ${
            status === 'error' && styles.error
          } ${status === 'succes' && styles.succes} ${
            status === 'warning' && styles.warning
          }`}
        >
          <img
            src={`/img/svg/${
              status === 'error' ? 'icn-warning' : 'icn-succes'
            }.svg`}
            alt=''
          />
          <p className='m-0'>{msg}</p>
          <img
            src={`/img/svg/icn-close.svg`}
            alt=''
            className={styles.toastNotification__close}
            onClick={() => setIsOpen(false)}
          />
        </div>
      )}
    </>
  );
};

export default ToastNotification;
