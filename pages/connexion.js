import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import SignInWith from '../containers/SignInWith/SignInWith';
import HeadTitle from '../containers/HeadTitle/HeadTitle';
import BackArrow from '../components/BackArrow/BackArrow';
import LoginForm from '../containers/LoginForm/LoginForm';
import ToastNotification from '../components/ToastNotification/ToastNotification';

const connexion = () => {
  const [errors, setErrors] = useState(null);
  const router = useRouter();

  useEffect(() => {
    if (localStorage.getItem('firstname')) {
      router.replace('/groupe');
    }
  }, []);

  return (
    <div className='container'>
      <div className='d-flex flex-column vh-100 justify-content-center'>
        <BackArrow />
        <HeadTitle title={'Connexion'} subtitle={''} />
        <LoginForm setErrors={setErrors} />
        <SignInWith />

        <p className='text-center m-0 mt-5 white-50'>
          Vous avez déja un compte ?
          <Link href='/inscription'>
            <a className='white font-weight-bold sm-font-size'> Inscription</a>
          </Link>
        </p>
      </div>
      {errors !== null && <ToastNotification msg={errors} />}
    </div>
  );
};

export default connexion;
