import React, { useEffect } from 'react';
import SingleEvent from '../SingleEvent/SingleEvent';

import styles from './EventList.module.scss';

const EventList = ({ events }) => {
  useEffect(() => {
    console.log(events);
  }, [events]);
  return (
    <div className={styles.EventList}>
      <h2>Événement à venir</h2>

      {events.map((ev) => {
        return <SingleEvent ev={ev} />;
      })}
    </div>
  );
};

export default EventList;
