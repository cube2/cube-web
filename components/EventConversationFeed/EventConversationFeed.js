import React, { useState, useEffect, useRef } from 'react';
import EventConversationInput from '../EventConversationInput/EventConversationInput';
import ConversationMessage from '../ConversationMessage/ConversationMessage';

import PopUp from '../PopUp/PopUp';

import styles from './EventConversationFeed.module.scss';
import Pusher from 'pusher-js';

const EventConversationFeed = ({ currentEvent, channel = null }) => {
  const [conversation, setConversation] = useState([]);
  const [userId, setUserId] = useState('');
  const messageListRef = useRef(null);
  const [errors, setErrors] = useState('');
  const [open, setOpen] = useState(false);

  const pusher = new Pusher(process.env.PUSHER_ID, {
    cluster: 'eu',
  });

  useEffect(() => {
    if (currentEvent) {
      setConversation(currentEvent.contents.reverse());
      const channel = pusher.subscribe(`event_${currentEvent.id}`);

      channel.bind('receiveMessage', (data) => {
        if (data.author.id !== localStorage.getItem('id')) {
          setConversation((conversation) => [
            ...conversation,
            {
              ...data,
            },
          ]);
        }
      });
    }
    console.log(currentEvent, 'ABC');
  }, [currentEvent]);

  useEffect(() => {
    if (conversation) {
      if (messageListRef.current) {
        messageListRef.current.scrollTo(0, messageListRef.current.scrollHeight);
      }
    }
  }, [conversation]);

  useEffect(() => {
    setUserId(localStorage.getItem('id'));
  }, []);

  return (
    <>
      <div className={styles.conversation} ref={messageListRef}>
        {conversation.map((message) => (
          <ConversationMessage
            message={message}
            userId={userId}
            key={Math.round()}
          />
        ))}
      </div>

      <EventConversationInput
        setConversation={setConversation}
        currentId={currentEvent.id}
        userId={userId}
        openEventModal={() => console.log('click')}
      />
    </>
  );
};

export default EventConversationFeed;
