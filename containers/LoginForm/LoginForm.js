import React, { useState } from 'react';
import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';
import axios from 'axios';
import { useRouter } from 'next/router';

const LoginForm = ({ setErrors }) => {
  const router = useRouter();

  const [form, setForm] = useState({
    email: {
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Adresse e-mail',
      },
      value: '',
      required: true,
      validationRules: {
        notEmpty: true,
      },
      errorMessage: {
        empty: 'Veuillez renseigner votre adresse mail',
      },
      className: 'mb-4',
    },

    password: {
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: 'Mot de passe',
      },
      value: '',
      required: true,
      validationRules: {
        notEmpty: true,
      },
      errorMessage: {
        empty: 'Veuillez renseigner votre mot de passe',
      },
      className: 'mb-4',
    },
  });

  const inputChangedHandler = (event, inputIdentifier) => {
    const updatedForm = {
      ...form,
    };

    const updatedFormElement = {
      ...updatedForm[inputIdentifier],
    };

    if (inputIdentifier === 'profile') {
      updatedFormElement.value = URL.createObjectURL(event.target.files[0]);
    } else {
      updatedFormElement.value = event.target.value;
    }

    updatedForm[inputIdentifier] = updatedFormElement;

    setForm(updatedForm);
  };

  const setErrorMessages = (msg = null, isValid) => {
    let err;
    if (!isValid) {
      err = msg;
      setErrors(err);
    } else {
      if (msg === err) {
        err = null;
        setErrors(err);
      }
    }
  };

  const checkValidity = (form) => {
    let isValid = true;
    for (const el in form) {
      if (form[el].validationRules.notEmpty) {
        isValid = isValid && form[el].value !== '';
        setErrorMessages(form[el].errorMessage.empty, form[el].value !== '');
      }
    }

    if (isValid) {
      console.log('send');
      axios
        .post(`${process.env.API_ENDPOINT}/auth/login`, {
          email: form.email.value,
          password: form.password.value,
        })
        .then((res) => {
          if (res.status === 200) {
            console.log('get infos');
            localStorage.setItem('firstname', res.data.firstname);
            localStorage.setItem('lastname', res.data.lastname);
            localStorage.setItem('id', res.data.id);
            localStorage.setItem('token', res.data.token);
            localStorage.setItem('picture', res.data.picture);
            if (res.data.role === 'Admin') {
              localStorage.setItem('role', res.data.role);
            }

            if (localStorage.getItem('waitingInvitation')) {
              router.push('/confirmInvitation');
            } else {
              router.push('/groupe');
            }
          } else {
            setErrorMessages('Email ou mot de passe incorrect', false);
          }
        });
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    checkValidity(form);
  };

  const formElementsArray = [];
  for (let key in form) {
    formElementsArray.push({
      id: key,
      config: form[key],
    });
  }

  return (
    <form className='mt-5' onSubmit={handleSubmit}>
      <div className='form-group m-0'>
        {formElementsArray.map((formElement) => (
          <Input
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            valid={formElement.config.valid}
            required={formElement.config.required}
            onChange={(event) => inputChangedHandler(event, formElement.id)}
            className={formElement.config.className}
          />
        ))}
        <div className='row justify-content-center'>
          <Button type='submit' className='white btn-submit py-2 px-4 mt-3'>
            Suivant
          </Button>
        </div>
      </div>
    </form>
  );
};

export default LoginForm;
