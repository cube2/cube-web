import React, { useState, useEffect } from 'react';
import VerticalElement from '../components/VerticalElement/VerticalElement';
import Layout from '../components/layout';
import LogoButton from '../components/LogoButton/LogoButton';
import TopBar from '../components/TopBar/TopBar';
import PopUp from '../components/PopUp/PopUp';

import axios from 'axios';
import Pusher from 'pusher-js';
import GroupCreate from '../containers/GroupCreate/GroupCreate';
import ToastNotification from '../components/ToastNotification/ToastNotification';
import PopularGroups from '../containers/PopularGroups/PopularGroups';
import Spinner from '../components/Spinner/Spinner';

const groupe = () => {
  const groupes = [
    {
      id: 1,
      picture: 'https://picsum.photos/200',
      name: 'Groupe Public 1',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      id: 2,
      picture: 'https://picsum.photos/200',
      name: 'Groupe Public 2',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      id: 3,
      picture: 'https://picsum.photos/200',
      name: 'Groupe Public 3',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      id: 4,
      picture: 'https://picsum.photos/200',
      name: 'Groupe Public 4',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      id: 5,
      picture: 'https://picsum.photos/200',
      name: 'Groupe Public 5',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
    {
      id: 6,
      picture: 'https://picsum.photos/200',
      name: 'Groupe Public 6',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },
  ];

  const [initialGroups, setInitialGroups] = useState([]);
  const [groups, setGroups] = useState([]);
  const [isFiltering, setIsFiltering] = useState(false);
  const [loading, setLoading] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const [errors, setErrors] = useState('');

  useEffect(() => {
    setLoading(true);
    const config = {
      method: 'get',
      url: `${process.env.API_ENDPOINT}/all_public_groups`,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };

    axios(config)
      .then((res) => {
        console.log(res, 'all groups');
        setGroups(res.data);
        setInitialGroups(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const filterGroups = (filter) => {
    if (filter === '') {
      setGroups(initialGroups);
      setIsFiltering(false);
    } else {
      console.log(groups);
      const searched = initialGroups.filter((group) =>
        group.name.toLowerCase().includes(filter.toLowerCase())
      );
      setGroups(searched);
      setIsFiltering(true);
    }
  };

  return (
    <Layout active={'groupe'} topBar={true}>
      <TopBar filter={filterGroups} />
      {loading ? (
        <Spinner className={'center'} />
      ) : (
        <>
          {!isFiltering ? (
            <>
              <PopularGroups
                title={'Les plus populaires'}
                groups={groups.slice(0, 4)}
              />
              <VerticalElement
                title={'A découvrir'}
                groupes={groups.slice(4, groups.length)}
              />
            </>
          ) : (
            <VerticalElement title={'Meilleur résultat'} groupes={groups} />
          )}
        </>
      )}
      <LogoButton
        icon={'icn-plus'}
        className={'add-group'}
        onClick={() => setIsOpen(true)}
      />
      <PopUp isOpen={isOpen} onClick={() => setIsOpen(false)}>
        <GroupCreate setErrors={setErrors} close={() => setIsOpen(false)} />
      </PopUp>

      {errors && <ToastNotification msg={errors} />}
    </Layout>
  );
};

export default groupe;
