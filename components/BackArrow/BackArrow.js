import React from 'react';
import styles from './BackArrow.module.scss';

const BackArrow = ({ onClick }) => {
  return (
    <div className={styles.backArrow}>
      <img
        src={`/img/svg/icn-back.svg`}
        onClick={onClick ? onClick : () => history.go(-1)}
      />
    </div>
  );
};

export default BackArrow;
