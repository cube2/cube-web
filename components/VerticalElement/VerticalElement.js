import React, { useState, useEffect } from 'react';
import styles from './VerticalElement.module.scss';
import PopUp from '../PopUp/PopUp';
import VerticalPublicGroup from '../VerticalPublicGroup/VerticalPublicGroup';

const verticalElement = ({ title, groupes }) => {
  return (
    <div className={styles.vertical}>
      <h2 onClick={() => setName('Bob')}>{title}</h2>

      <div className={`${styles.verticalElement} row w-100 mx-0`}>
        {groupes.map((group) => {
          return <VerticalPublicGroup group={group} />;
        })}
      </div>
    </div>
  );
};

export default verticalElement;
