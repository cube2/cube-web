import React, { useState, useEffect } from 'react';
import styles from './Search.module.scss';

const Search = ({ onChange }) => {
  const [filter, setFilter] = useState('');

  const handleChange = (e) => {
    setFilter(e.target.value);
  };

  useEffect(() => {
    onChange(filter);
  }, [filter]);

  return (
    <div className={styles.search}>
      <img src={`/img/svg/icn-search.svg`} alt='' />
      <input
        type='text'
        value={filter}
        onChange={handleChange}
        placeholder={'Rechercher'}
      />
    </div>
  );
};

export default Search;
