import React, { useState, useEffect, useRef } from 'react';
import ConversationInput from '../ConversationInput/ConversationInput';
import ConversationMessage from '../ConversationMessage/ConversationMessage';

import PopUp from '../PopUp/PopUp';
import EventCreate from '../../containers/EventCreate/EventCreate';

import styles from './ConversationFeed.module.scss';

const ConversationFeed = ({ currentGroup, channel }) => {
  const [conversation, setConversation] = useState([]);
  const [userId, setUserId] = useState('');
  const messageListRef = useRef(null);
  const [errors, setErrors] = useState('');
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (currentGroup) {
      setConversation(currentGroup.contents.reverse());
    }
  }, [currentGroup]);

  useEffect(() => {
    if (channel) {
      channel.bind('receiveMessage', (data) => {
        if (data.author.id !== localStorage.getItem('id')) {
          setConversation((conversation) => [
            ...conversation,
            {
              ...data,
            },
          ]);
        }
      });
    }
  }, [channel]);

  useEffect(() => {
    if (conversation) {
      if (messageListRef.current) {
        messageListRef.current.scrollTo(0, messageListRef.current.scrollHeight);
      }
    }
  }, [conversation]);

  useEffect(() => {
    setUserId(localStorage.getItem('id'));
  }, []);

  return (
    <>
      <div className={styles.conversation} ref={messageListRef}>
        {conversation.map((message) => (
          <ConversationMessage message={message} userId={userId} />
        ))}
      </div>

      <ConversationInput
        setConversation={setConversation}
        currentId={currentGroup.id}
        userId={userId}
        openEventModal={() => setOpen(true)}
      />
      <PopUp isOpen={open} onClick={() => setOpen(false)}>
        <EventCreate
          setErrors={setErrors}
          currentId={currentGroup.id}
          close={() => setOpen(false)}
        />
      </PopUp>
    </>
  );
};

export default ConversationFeed;
