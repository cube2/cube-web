import React, { useState } from 'react';
import { useRouter } from 'next/router';
import Button from '../../components/Button/Button';
import styles from './AdminConnexion.module.scss';

const AdminConnexion = () => {
  const [identifiant, setIdentifiant] = useState('');
  const [mdp, setMdp] = useState('');
  const [errors, setErrors] = useState('');

  const router = useRouter();

  const logIn = () => {
    if (identifiant === 'admin' && mdp === 'admin') {
      router.push('/dashboard');
    } else {
    }
  };

  return (
    <div className={styles.AdminConnexion}>
      <div className={styles.AdminConnexion__pannel}>
        <div className={styles.AdminConnexion__pannel__identifiant}>
          <p>Identifiant :</p>
          <input
            type='text'
            value={identifiant}
            onChange={(e) => setIdentifiant(e.target.value)}
          />
        </div>
        <div className={styles.AdminConnexion__pannel__mdp}>
          <p>Mot de passe :</p>
          <input
            type='password'
            value={mdp}
            onChange={(e) => setMdp(e.target.value)}
          />
        </div>
        <div className={styles.AdminConnexion__pannel__btn}>
          <Button
            type='submit'
            className='white btn-submit-green py-2 px-4 mt-3'
            onClick={logIn}
          >
            Connexion
          </Button>
        </div>
      </div>
    </div>
  );
};

export default AdminConnexion;
