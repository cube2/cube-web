import React, { useEffect, useState } from 'react';
import styles from './TopBar.module.scss';

import Profile from '../Profile/Profile';
import Search from '../Search/Search';

const TopBar = ({ filter }) => {
  const [picture, setPicture] = useState();

  useEffect(() => {
    setPicture(localStorage.getItem('picture'));
  }, []);

  return (
    <div className={styles.topBar}>
      <Search onChange={filter} />
      <Profile url={picture} name='Nom Prénom' />
    </div>
  );
};

export default TopBar;
