import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';

const confirmInvitation = ({ query }) => {
  const router = useRouter();
  useEffect(() => {
    console.log(query.id);
    if (localStorage.getItem('firstname')) {
      axios
        .post(`${process.env.API_ENDPOINT}/confirm_invitation`, {
          token: localStorage.getItem('token'),
          id: localStorage.getItem('id'),
          id_group: query.id,
        })
        .then((res) => {
          router.push('/groupe');
        });
    } else {
      localStorage.setItem('waitingInvitation', true);
      router.push('/');
    }
  }, []);
  return <div>Spinner</div>;
};

confirmInvitation.getInitialProps = ({ query }) => {
  return { query };
};

export default confirmInvitation;
