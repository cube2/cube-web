import React, { useState } from 'react';
import styles from './Radio.module.scss';

const Radio = ({
  selected,
  onChange,
  text,
  value,
  selfActiveOnClick = false,
}) => {
  const [active, setActive] = useState(false);

  if (!selfActiveOnClick) {
    return (
      <div
        className={styles.radio}
        onClick={() => {
          onChange(value);
        }}
      >
        <div
          className={`${styles.radio__outerCircle} ${
            value !== selected && styles.radio__unselected
          }`}
        >
          <div
            className={`${styles.radio__outerCircle__innerCircle} ${
              value !== selected && styles.radio__outerCircle__unselectedCircle
            }`}
          />
        </div>
      </div>
    );
  } else {
    return (
      <div
        className={styles.radio}
        onClick={() => {
          setActive((active) => !active);
          onChange();
        }}
      >
        <div
          className={`${styles.radio__outerCircle} ${
            !active && styles.radio__unselected
          }`}
        >
          <div
            className={`${styles.radio__outerCircle__innerCircle} ${
              !active && styles.radio__outerCircle__unselectedCircle
            }`}
          />
        </div>
      </div>
    );
  }
};

export default Radio;
