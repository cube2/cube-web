import React, { useEffect } from 'react';
import axios from 'axios';

import { useRouter } from 'next/router';
import styles from '../styles/EmailConfirmed.module.scss';

const EmailConfirmed = ({ query }) => {
  const router = useRouter();

  useEffect(() => {
    console.log(query);
    axios
      .get(`${process.env.API_ENDPOINT}/emailConfirmed/${query.id}`)
      .then((res) => {
        if (res.data.status === 202) {
          localStorage.setItem('firstname', res.data.user.firstname);
          localStorage.setItem('lastname', res.data.user.lastname);
          localStorage.setItem('id', res.data.user.id);
          localStorage.setItem('token', res.data.user.token);
          localStorage.setItem('picture', res.data.user.picture);
          if (res.data.user.role === 'Admin') {
            localStorage.setItem('role', res.data.user.role);
          }
          router.push('/groupe');
        } else {
          //TODO: ERROR
        }
      });
  }, []);

  return (
    <div className={styles.EmailConfirmed}>
      <div className={styles.EmailConfirmed__box}>
        <img src='/img/smallLogo.png' alt='' />
        <p>
          Votre compte est à présent validé. Vous allez etre redirigé vers la
          page d'accueil
        </p>
      </div>
    </div>
  );
};

EmailConfirmed.getInitialProps = ({ query }) => {
  return { query };
};

export default EmailConfirmed;
