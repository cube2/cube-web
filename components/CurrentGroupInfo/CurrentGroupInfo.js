import React, { useState, useEffect } from 'react';
import PopUp from '../PopUp/PopUp';
import EventCreate from '../../containers/EventCreate/EventCreate';
import axios from 'axios';
import styles from './CurrentGroupInfo.module.scss';
import Link from 'next/link';

const CurrentGroupInfo = ({ currentId, currentGroup }) => {
  const [open, setOpen] = useState(false);
  const [members, setMembers] = useState([]);
  const [events, setEvents] = useState([]);
  const [loading, setLoading] = useState(true);
  const [errors, setErrors] = useState('');

  useEffect(() => {
    if (currentId) {
      setLoading(true);
      const config = {
        method: 'get',
        url: `${process.env.API_ENDPOINT}/get_group/${currentId}`,
        headers: {
          token: localStorage.getItem('token'),
          id: localStorage.getItem('id'),
        },
      };

      axios(config).then((res) => {
        setMembers(res.data.members);
        setEvents(res.data.events);

        setLoading(false);
      });
    }
  }, [currentId]);

  return (
    <>
      {!loading && (
        <div className={styles.currentGroupInfo}>
          {currentGroup && (
            <div className={styles.currentGroupInfo__box}>
              <img
                src={currentGroup.picture}
                alt=''
                className={styles.currentGroupInfo__box__picture}
              />

              <div className={styles.currentGroupInfo__box__members}>
                <p>Membres du groupe</p>
                <div className={styles.currentGroupInfo__box__members__list}>
                  <div
                    className={
                      styles.currentGroupInfo__box__members__list__creator
                    }
                  >
                    <div
                      className={
                        styles.currentGroupInfo__box__members__list__creator__img
                      }
                    >
                      <img
                        src={members[0].author.picture}
                        alt='
                  '
                      />
                    </div>
                    <p>{members[0].author.firstname}</p>
                  </div>
                  <div
                    className={
                      styles.currentGroupInfo__box__members__list__others
                    }
                  >
                    {members.map((member, i) => {
                      if (i > 0) return <img src={member.author.picture} />;
                    })}
                  </div>
                </div>
              </div>

              <div className={styles.currentGroupInfo__box__event}>
                <div className={styles.currentGroupInfo__box__event__header}>
                  <p>Evenements</p>
                  <div
                    className={styles.currentGroupInfo__box__event__header__btn}
                    onClick={() => setOpen(true)}
                  >
                    <img src='/img/svg/icn-plus.svg' alt='' />
                  </div>
                </div>
                <div className={styles.currentGroupInfo__box__event__list}>
                  {events.map((ev) => (
                    <Link href={`/evenement?eventId=${ev.id}`}>
                      <a className='text-decoration-none'>
                        <div
                          className={
                            styles.currentGroupInfo__box__event__list__single
                          }
                        >
                          <img src={ev.picture} alt='' />
                          <p>{ev.name}</p>
                        </div>
                      </a>
                    </Link>
                  ))}
                </div>
              </div>
            </div>
          )}
        </div>
      )}
      <PopUp isOpen={open} onClick={() => setOpen(false)}>
        <EventCreate
          setErrors={setErrors}
          currentId={currentId}
          close={() => setOpen(false)}
        />
      </PopUp>
    </>
  );
};

export default CurrentGroupInfo;
