import React, { useState } from 'react';
import styles from './Input.module.scss';

const Input = (props) => {
  const [file, setFile] = useState(null);
  let inputElement = null;

  const handleChange = (event) => {
    setFile(URL.createObjectURL(event.target.files[0]));
    props.onChange(event);
  };

  switch (props.elementType) {
    case 'input':
      inputElement = (
        <div className={props.className}>
          <input
            className='form-control inp inp-border py-4'
            value={props.value}
            onChange={props.onChange}
            {...props.elementConfig}
          />
        </div>
      );
      break;

    case 'file':
      inputElement = (
        <div className={props.className}>
          <label className={styles.inputFile}>
            <input {...props.elementConfig} onChange={handleChange} />
            <img src={file} border='0' />
          </label>
        </div>
      );
      break;

    default:
      inputElement = (
        <input
          className='form-control inp inp-border'
          value={props.value}
          onChange={props.onChange}
          {...props.elementConfig}
        />
      );
      break;
  }
  return (
    <div className={props.col}>
      <div className='form-group'>{inputElement}</div>
    </div>
  );
};

export default Input;
