import React, { useState, useEffect, useRef } from 'react';
import PopUp from '../PopUp/PopUp';
import axios from 'axios';
import Button from '../Button/Button';
import styles from './VerticalPublicGroup.module.scss';
import { useRouter } from 'next/router';

const VerticalPublicGroup = ({ group }) => {
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const [members, setMembers] = useState([]);
  const [isMember, setIsMember] = useState(false);

  useEffect(() => {
    console.log(group, 'single');
    const config = {
      method: 'get',
      url: `${process.env.API_ENDPOINT}/get_group/${group._id}`,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };

    axios(config).then((res) => {
      console.log(res);
      setMembers(res.data.members);
    });
  }, []);

  useEffect(() => {
    if (members.length) {
      members.map((member) => {
        if (member.author.id === localStorage.getItem('id')) {
          setIsMember(true);
        }
      });
    }
  }, [members]);

  const joinGroup = (id) => {
    const config = {
      method: 'post',
      url: `${process.env.API_ENDPOINT}/add_members`,
      headers: {
        token: localStorage.getItem('token'),
        id: localStorage.getItem('id'),
      },
      data: {
        id: id,
        user_ids: [localStorage.getItem('id')],
      },
    };

    axios(config).then((res) => {
      if (res.data.status === 202) {
        router.push(`/conversations?id=${group.id}`);
      }
    });
  };

  return (
    <>
      <div
        key={group.id}
        className={styles.VerticalPublicGroup}
        onClick={() => setIsOpen(true)}
      >
        <img src={group.picture} alt='' />
        <h3>{group.name}</h3>
        <p>
          {group.description.length > 200
            ? `${group.description.substr(0, 200)} ...`
            : group.description}
        </p>
      </div>
      <PopUp isOpen={isOpen} onClick={() => setIsOpen(false)}>
        <div className={styles.joinPopup}>
          <img src={group.picture} alt='' />
          <h3>{group.name}</h3>
          <p>
            {group.description.length > 200
              ? `${group.description.substr(0, 200)} ...`
              : group.description}
          </p>
          <div className={styles.joinPopup__members}>
            <p>{members.length} membres</p>
            <div className={styles.joinPopup__members__pictureList}>
              {members.slice(0, 3).map((member) => (
                <img src={member.author.picture} />
              ))}
              {members.length > 3 && (
                <div className={styles.joinPopup__members__pictureList__other}>
                  {members.length - 3}
                </div>
              )}
            </div>
          </div>

          {isMember ? (
            <p className={styles.joinPopup__already}>
              Vous faites déja parti de ce groupe
            </p>
          ) : (
            <Button
              type='submit'
              className='white btn-submit-green py-2 px-4 mt-3'
              onClick={() => {
                joinGroup(group.id);
              }}
            >
              Rejoindre
            </Button>
          )}
        </div>
      </PopUp>
    </>
  );
};

export default VerticalPublicGroup;
