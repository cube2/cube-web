import React, { useEffect, useRef } from 'react';
import Link from 'next/link';
import styles from './PrivateMessageList.module.scss';
import HorizontalElement from '../HorizontalElement/HorizontalElement';

const PrivateMessageList = ({ privateConversations }) => {
  return (
    <HorizontalElement>
      <div className={styles.privateList__item}>
        <div
          className={`${styles.privateList__item__box} ${styles.privateList__item__add}`}
        >
          <img src='/img/svg/icn-plus.svg' alt='' />
        </div>
      </div>

      {privateConversations.map((conversations, i) => (
        <div className={styles.privateList__item}>
          <div
            className={`${styles.privateList__item__box}`}
            style={{
              background: `url(${conversations.picture}) center / cover`,
            }}
          ></div>
          <p>{conversations.name}</p>
        </div>
      ))}

      <div className={styles.privateList__spacer}></div>
    </HorizontalElement>
  );
};

export default PrivateMessageList;
