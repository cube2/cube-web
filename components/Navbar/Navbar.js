import React from 'react';
import styles from './Navbar.module.scss';
import Link from 'next/link';
import { useRouter } from 'next/router';

const navbar = ({ small, active, items }) => {
  const router = useRouter();

  const logOut = () => {
    localStorage.clear();
    router.push('/');
  };

  return (
    <nav className={`${styles.navbar} ${small ? styles.small : ''}`}>
      <ul className={`navbar-nav ${styles.navbarNav}`}>
        {items.map((item) => (
          <Link href={item.slug} key={item.slug}>
            <a className='w-100 text-decoration-none'>
              <li
                className={`nav-item ${
                  styles.navItem
                } my-lg-4 d-flex align-items-center px-2 ${
                  active === item.slug ? styles.active : ''
                }`}
              >
                <img
                  src={`img/svg/icn-${item.icon}.svg`}
                  alt=''
                  className='mr-sm-4'
                />
                <p className={`nav-link pb-0 m-0`}>{item.label}</p>
              </li>
            </a>
          </Link>
        ))}

        <li
          className={`nav-item ${styles.navItem} my-lg-4 d-flex align-items-center px-2 ${styles.logOut}`}
          onClick={logOut}
        >
          <img src={`img/svg/icn-logOut.svg`} alt='' className='mr-sm-4' />
          <p className={`nav-link pb-0 m-0`}>Déconnexion</p>
        </li>
      </ul>
    </nav>
  );
};

export default navbar;
