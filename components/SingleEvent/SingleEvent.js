import React from 'react';
import styles from './SingleEvent.module.scss';
import * as dayjs from 'dayjs';
import Link from 'next/link';

const SingleEvent = ({ ev }) => {
  const formatDate = (date) => {
    const day = dayjs(date).date();
    let month = dayjs(date).month();

    switch (month) {
      case 0:
        month = 'Jan';
        break;
      case 1:
        month = 'Fev';
        break;
      case 2:
        month = 'Mar';
        break;
      case 3:
        month = 'Avr';
        break;
      case 4:
        month = 'Mai';
        break;
      case 5:
        month = 'Juin';
        break;
      case 6:
        month = 'Juil';
        break;
      case 7:
        month = 'Aout';
        break;
      case 8:
        month = 'Sep';
        break;
      case 9:
        month = 'Oct';
        break;
      case 10:
        month = 'Nov';
        break;
      case 11:
        month = 'Déc';
        break;
    }

    return `${day} ${month}`;
  };
  return (
    <Link href={`/evenement?eventId=${ev.id}`}>
      <a className='text-decoration-none'>
        <div className={styles.SingleEvent}>
          <div className={styles.SingleEvent__desc}>
            <img src={ev.picture} alt='' />
            <div className={styles.SingleEvent__desc__name}>
              <p>{ev.name}</p>
              <p>{ev.description}</p>
            </div>
          </div>
          <div className={styles.SingleEvent__info}>
            <div className={styles.SingleEvent__info__date}>
              <p>Date</p>
              <p>{formatDate(ev.date_event)}</p>
            </div>
            <div className={styles.SingleEvent__info__location}>
              <p>Lieu</p>
              <p>{ev.location}</p>
            </div>
            <div className={styles.SingleEvent__info__author}>
              <p>Par</p>
              <img src={ev.author.picture} alt='' />
            </div>
          </div>
        </div>
      </a>
    </Link>
  );
};

export default SingleEvent;
