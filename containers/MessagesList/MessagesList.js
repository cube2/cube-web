import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import styles from './MessagesList.module.scss';
import CurrentGroupHeader from '../../components/CurrentGroupHeader/CurrentGroupHeader';
import ConversationFeed from '../../components/ConversationFeed/ConversationFeed';
import Spinner from '../../components/Spinner/Spinner';

const MessagesList = ({ setCurrentId, currentId, channel }) => {
  const [currentGroup, setCurrentGroup] = useState(null);
  const [loading, setLoading] = useState(true);

  const containerRef = useRef(null);
  const firstUpdate = useRef(true);

  useEffect(() => {
    if (currentId) {
      setLoading(true);
      const config = {
        method: 'get',
        url: `${process.env.API_ENDPOINT}/get_group/${currentId}`,
        headers: {
          token: localStorage.getItem('token'),
          id: localStorage.getItem('id'),
        },
      };

      axios(config).then((res) => {
        console.log(res, 'current');
        setCurrentGroup(res.data);
      });
    }
  }, [currentId]);

  useEffect(() => {
    if (currentId) {
      containerRef.current.classList.add('test');
    } else {
      containerRef.current.classList.remove('test');
    }
  }, [currentId]);

  useEffect(() => {
    if (currentGroup) {
      setLoading(false);
    }
  }, [currentGroup]);

  return (
    <div className={styles.MessagesList} ref={containerRef}>
      {loading ? (
        <Spinner className={'fixed'} />
      ) : (
        <>
          <CurrentGroupHeader
            currentGroup={currentGroup}
            setCurrentId={setCurrentId}
          />
          <ConversationFeed currentGroup={currentGroup} channel={channel} />
        </>
      )}
    </div>
  );
};

export default MessagesList;
