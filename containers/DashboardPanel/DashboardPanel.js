import React, { useEffect, useState } from 'react';
import styles from './DashboardPanel.module.scss';
import axios from 'axios';

const DashboardPanel = () => {
  const [allGroups, setAllGroups] = useState([]);
  const [allUsers, setAllusers] = useState([]);

  useEffect(() => {
    const config = {
      method: 'get',
      url: `${process.env.API_ENDPOINT}/all_groups`,
      headers: {
        token: localStorage.getItem('token'),
        id: localStorage.getItem('id'),
      },
    };

    axios(config)
      .then((res) => {
        console.log(res, 'all group');
        setAllGroups(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    const config = {
      method: 'get',
      url: `${process.env.API_ENDPOINT}/get_active_users`,
      headers: {
        token: localStorage.getItem('token'),
        id: localStorage.getItem('id'),
      },
    };

    axios(config)
      .then((res) => {
        console.log(res);
        setAllusers(res.data);
      })
      .catch((error) => {
        console.log(error);
        setErrors(error);
      });
  }, []);

  const deleteGroup = (id) => {
    const config = {
      method: 'delete',
      url: `${process.env.API_ENDPOINT}/delete_group/${id}`,
      headers: {
        token: localStorage.getItem('token'),
        id: localStorage.getItem('id'),
      },
    };

    axios(config)
      .then((res) => {
        console.log(res);
        if (res.data.status === 202) {
          let updatedGroup = [...allGroups];
          updatedGroup = updatedGroup.filter((group) => group.id !== id);
          setAllGroups(updatedGroup);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const deleteUser = (id) => {
    const config = {
      method: 'delete',
      url: `${process.env.API_ENDPOINT}/delete_user/${id}`,
      headers: {
        token: localStorage.getItem('token'),
        id: localStorage.getItem('id'),
      },
    };

    axios(config)
      .then((res) => {
        console.log(res);
        if (res.data.status === 202) {
          let updatedUser = [...allUsers];
          updatedUser = updatedUser.filter((user) => user.id !== id);
          setAllUsers(updatedUser);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <h2 className={styles.title}>Dashboard</h2>
      <div className={styles.DashboardPanel}>
        <div className={styles.DashboardPanel__left}>
          <div className={styles.DashboardPanel__left__createGroup}>
            <p className='m-4'>
              Utilisateurs de l'application : {allUsers.length}
            </p>
            <p className='m-4'>Groupes crées : {allGroups.length}</p>
          </div>
          <div className={styles.DashboardPanel__left__listMember}>
            {allUsers.map((user) => (
              <div
                className={styles.DashboardPanel__left__listMember__member}
                key={user.id}
              >
                <div
                  className={
                    styles.DashboardPanel__left__listMember__member__image
                  }
                >
                  <img src={user.picture} alt='' />
                </div>
                <p>
                  {user.firstname} {user.lastname}
                </p>
                <img
                  src='/img/svg/icn-delete.svg'
                  alt=''
                  onClick={() => {
                    deleteUser(user.id);
                  }}
                />
              </div>
            ))}
          </div>
        </div>
        <div className={styles.DashboardPanel__right}>
          <div className={styles.DashboardPanel__right__listGroup}>
            {allGroups.map((group) => (
              <div
                className={styles.DashboardPanel__right__listGroup__group}
                key={group.id}
              >
                <div
                  className={
                    styles.DashboardPanel__right__listGroup__group__image
                  }
                >
                  <img src={group.picture} alt='' />
                </div>
                <p>{group.name}</p>
                <img
                  src='/img/svg/icn-delete.svg'
                  alt=''
                  onClick={() => deleteGroup(group.id)}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default DashboardPanel;
