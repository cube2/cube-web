import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styles from './CurrentEventInfo.module.scss';
import * as dayjs from 'dayjs';

const CurrentEventInfo = ({ currentId }) => {
  const [currentEvent, setcurrentEvent] = useState(null);
  const [members, setMembers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (currentId) {
      setLoading(true);
      const config = {
        method: 'get',
        url: `${process.env.API_ENDPOINT}/get_event/${currentId}`,
        headers: {
          token: localStorage.getItem('token'),
          id: localStorage.getItem('id'),
        },
      };

      axios(config).then((res) => {
        setcurrentEvent(res.data);
        setMembers(res.data.participants);
        setLoading(false);

        console.log(res, 'AHHHHHHHHHHHHHHH');
      });
    }
  }, [currentId]);

  const formatDate = (date) => {
    const day = dayjs(date).date();
    let month = dayjs(date).month();
    const year = dayjs(date).year();

    switch (month) {
      case 0:
        month = 'Janvier';
        break;
      case 1:
        month = 'Février';
        break;
      case 2:
        month = 'Mars';
        break;
      case 3:
        month = 'Avril';
        break;
      case 4:
        month = 'Mai';
        break;
      case 5:
        month = 'Juin';
        break;
      case 6:
        month = 'Juillet';
        break;
      case 7:
        month = 'Aout';
        break;
      case 8:
        month = 'Septembre';
        break;
      case 9:
        month = 'Octobre';
        break;
      case 10:
        month = 'Novembre';
        break;
      case 11:
        month = 'Décembre';
        break;
    }

    return `${day} ${month} ${year}`;
  };

  return (
    <>
      {!loading && (
        <div className={styles.currentGroupInfo}>
          <div className={styles.currentGroupInfo__box}>
            <img
              src={currentEvent.picture}
              alt=''
              className={styles.currentGroupInfo__box__picture}
            />

            <div className={styles.currentGroupInfo__box__members}>
              <p>Participants de l'évènement</p>
              <div className={styles.currentGroupInfo__box__members__list}>
                <div
                  className={
                    styles.currentGroupInfo__box__members__list__creator
                  }
                >
                  <div
                    className={
                      styles.currentGroupInfo__box__members__list__creator__img
                    }
                  >
                    <img
                      src={members[0].author.picture}
                      alt='
                  '
                    />
                  </div>
                  <p>{members[0].author.firstname}</p>
                </div>
                <div
                  className={
                    styles.currentGroupInfo__box__members__list__others
                  }
                >
                  {members.map((member, i) => {
                    if (i > 0)
                      return (
                        <img
                          src={member.author.picture}
                          key={member.author.id}
                        />
                      );
                  })}
                  <img src='https://i.picsum.photos/id/103/2592/1936.jpg?hmac=aC1FT3vX9bCVMIT-KXjHLhP6vImAcsyGCH49vVkAjPQ' />
                </div>
              </div>
            </div>
            <div className={styles.currentGroupInfo__box__info}>
              <div className={styles.currentGroupInfo__box__info__title}>
                <p>Informations utiles :</p>
              </div>
              <div className={styles.currentGroupInfo__box__info__date}>
                <p>Date</p>
                <p>{formatDate(currentEvent.date_event)}</p>
              </div>
              <div className={styles.currentGroupInfo__box__info__lieu}>
                <p>Lieu</p>
                <p>{currentEvent.location}</p>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default CurrentEventInfo;
