import React from 'react';
import { render, screen } from './test-utils';
import Home from '../pages/index';

describe('HomePage', () => {
  it('should render the heading', () => {
    const textToFind = 'Agora';

    render(<Home />);
    const heading = screen.getByText(textToFind);

    expect(heading).toBeInTheDocument();
  });

  it('should render the logo', () => {
    render(<Home />);
    const img = document.querySelector('.logoAgora');
    expect(img).toBeInTheDocument();
  });

  it('should render the buttons', () => {
    render(<Home />);
    const btns = document.querySelector('.btns');
    expect(btns).toBeInTheDocument();
  });

  it('should render the IOS and Android download buttons', () => {
    render(<Home />);
    const stores = document.querySelector('.dl');
    expect(stores).toBeInTheDocument();
  });
});
