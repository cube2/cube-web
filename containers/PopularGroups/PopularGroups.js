import React, { useState } from 'react';
import Link from 'next/link';
import styles from './PopularGroups.module.scss';
import HorizontalElement from '../../components/HorizontalElement/HorizontalElement';
import PublicGroup from '../../components/PublicGroup/PublicGroup';

const PopularGroups = ({ title, groups }) => {
  return (
    <>
      <h2 className={styles.title}>{title}</h2>
      <HorizontalElement>
        {groups.map((group, i) => {
          return <PublicGroup group={group} />;
        })}
        <div className={styles.PopularGroups__divider}></div>
      </HorizontalElement>
    </>
  );
};

export default PopularGroups;
