import React, { useEffect, useState } from 'react';
import styles from './EventList.module.scss';
import axios from 'axios';
import * as dayjs from 'dayjs';
import Spinner from '../../components/Spinner/Spinner';

const EventList = () => {
  const [events, setEvents] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const config = {
      method: 'get',
      url: `${process.env.API_ENDPOINT}/user_future_events`,
      headers: {
        token: localStorage.getItem('token'),
        id: localStorage.getItem('id'),
      },
    };

    axios(config).then((res) => {
      console.log(res, 'all');
      setEvents(res.data);
      setLoading(false);
    });
  }, []);

  const formatDate = (date) => {
    const day = dayjs(date).date();
    let month = dayjs(date).month();

    switch (month) {
      case 0:
        month = 'Jan';
        break;
      case 1:
        month = 'Fev';
        break;
      case 2:
        month = 'Mar';
        break;
      case 3:
        month = 'Avr';
        break;
      case 4:
        month = 'Mai';
        break;
      case 5:
        month = 'Juin';
        break;
      case 6:
        month = 'Juil';
        break;
      case 7:
        month = 'Aout';
        break;
      case 8:
        month = 'Sep';
        break;
      case 9:
        month = 'Oct';
        break;
      case 10:
        month = 'Nov';
        break;
      case 11:
        month = 'Déc';
        break;
    }

    return `${day} ${month}`;
  };

  return (
    <div className={styles.EventList}>
      {loading ? (
        <Spinner className={'center'} />
      ) : (
        <>
          <h2>Évènements à venir</h2>
          <div className={styles.EventList__events}>
            {events.map((ev) => (
              <div className={styles.EventList__events__single}>
                <img
                  src={ev.group_picture}
                  alt=''
                  className={styles.EventList__events__single__groupImage}
                />
                <div className={styles.EventList__events__single__desc}>
                  <img src={ev.picture} alt='' />
                  <div className={styles.EventList__events__single__desc__name}>
                    <p>{ev.name}</p>

                    <p>{ev.description}</p>
                  </div>
                </div>
                <div className={styles.EventList__events__single__info}>
                  <div className={styles.EventList__events__single__info__date}>
                    <p>Date :</p>
                    <p>{formatDate(ev.date_event)}</p>
                  </div>
                  <div
                    className={styles.EventList__events__single__info__location}
                  >
                    <p>Lieu :</p>
                    <p>{ev.location}</p>
                  </div>
                  <div
                    className={styles.EventList__events__single__info__author}
                  >
                    <p>Par :</p>
                    <img src={ev.author.picture} alt='' />
                  </div>
                </div>
              </div>
            ))}
          </div>
        </>
      )}
    </div>
  );
};

export default EventList;
