import React, { useState, useEffect } from 'react';
import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';
import BackArrow from '../../components/BackArrow/BackArrow';
import axios from 'axios';
import { useRouter } from 'next/router';
import Compressor from 'compressorjs';

const StepForm = ({
  step,
  setStep,
  name,
  setName,
  email,
  setEmail,
  phone,
  setPhone,
  errors,
  setErrors,
}) => {
  const [formIsValid, setFormIsValid] = useState(false);
  const router = useRouter();

  let err = errors;

  const [form, setForm] = useState({
    firstname: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Prénom',
      },
      value: '',
      required: true,
      validationRules: {
        notEmpty: true,
      },
      valid: false,
      errorMessage: {
        empty: 'Veuillez renseigner votre prénom',
      },
      step: 1,
      className: 'mb-4',
    },
    lastname: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Nom',
      },
      value: '',
      required: true,
      validationRules: {
        notEmpty: true,
      },
      valid: false,
      errorMessage: {
        empty: 'Veuillez renseigner votre nom',
      },
      step: 1,
      className: 'mb-4',
    },
    email: {
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Adresse e-mail',
      },
      value: '',
      required: true,
      validationRules: {
        notEmpty: true,
        isEmail: true,
      },
      errorMessage: {
        empty: 'Veuillez renseigner votre adresse mail',
        syntax:
          "Veuillez respecter le format d'un courriel (exemple@domaine.com)",
        exist: "L'adresse mail renseigné est déja utilisé",
      },
      valid: false,
      className: 'mb-4',
      step: 2,
    },
    password: {
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: 'Mot de passe',
      },
      value: '',
      required: true,
      validationRules: {
        notEmpty: true,
      },
      errorMessage: {
        empty: 'Veuillez renseigner un mot de passe',
      },
      valid: false,
      className: 'mb-4',
      step: 2,
    },
    confirmedPassword: {
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: 'Confirmer le mot de passe',
      },
      value: '',
      required: true,
      validationRules: {
        notEmpty: true,
        isEqual: true,
      },
      valid: false,
      errorMessage: {
        empty: 'Veuillez confirmer votre mot de passe',
        notEqual: 'Les deux mot de passe saisie sont différent',
      },
      className: 'mb-4',
      step: 2,
    },
    phone: {
      elementType: 'input',
      elementConfig: {
        type: 'tel',
        placeholder: 'Numéro de téléphone',
      },
      value: '',
      required: false,
      validationRules: {
        isPhone: true,
      },
      errorMessage: {
        notPhone: 'Le numéro de téléphone est incorrect',
      },
      valid: true,
      className: 'mb-4',
      step: 3,
    },
    profile: {
      elementType: 'file',
      elementConfig: {
        type: 'file',
      },
      value: '',
      required: true,
      validationRules: {
        notEmpty: true,
      },
      errorMessage: {
        empty: "Aucune photo de profil n'a été renseigné",
      },
      valid: false,
      className: 'mb-4 d-flex justify-content-center',
      step: 4,
    },
  });

  useEffect(() => {
    if (formIsValid) {
      if (step < 4) {
        _next();
      } else {
        submitForm();
      }
    }
  }, [formIsValid]);

  const makeCompressor = (file, options) => {
    return new Compressor(file, options);
  };

  const inputChangedHandler = (event, inputIdentifier) => {
    const updatedForm = {
      ...form,
    };

    const updatedFormElement = {
      ...updatedForm[inputIdentifier],
    };

    if (inputIdentifier === 'profile') {
      updatedFormElement.value = event.target.files[0];
    } else {
      updatedFormElement.value = event.target.value;
    }

    updatedForm[inputIdentifier] = updatedFormElement;

    setForm(updatedForm);
  };

  const setErrorMessages = (msg = null, isValid) => {
    if (!isValid) {
      err = msg;
      setErrors(err);
    } else {
      if (msg === err) {
        console.log('oui');
        err = null;
        setErrors(err);
      }
      if (isValid === 'goodEmail') {
        err = null;
        setErrors(err);
      }
    }
  };

  const checkValidity = async (tabEl) => {
    let isValid = true;
    console.log(tabEl, 'tabEl');

    for (const el in tabEl) {
      if (tabEl[el].validationRules.notEmpty) {
        isValid = isValid && tabEl[el].value !== '';
        setErrorMessages(tabEl[el].errorMessage.empty, tabEl[el].value !== '');
      }

      if (tabEl[el].validationRules.isEmail) {
        console.log(tabEl[el].value);
        console.log(`${process.env.API_ENDPOINT}/auth/check_email`);
        const res = await axios.post(
          `${process.env.API_ENDPOINT}/auth/check_email`,
          {
            email: tabEl[el].value,
          }
        );

        isValid = isValid && res.status === 202;
        if (res.status === 202) {
          setErrorMessages(res.data.status === 202, 'goodEmail');
        } else if (res.status === 403) {
          setErrorMessages(tabEl[el].errorMessage.syntax, res.status === 202);
        } else if (res.status === 404) {
          setErrorMessages(tabEl[el].errorMessage.exist, res.status === 202);
        }
      }

      if (tabEl[el].validationRules.isPhone) {
        console.log(tabEl[el], 'tabEl');
        if (tabEl[el].value !== '') {
          const regex = /^((\+)33|0)[1-9](\d{2}){4}$/;
          isValid = isValid && regex.test(tabEl[el].value);

          setErrorMessages(
            tabEl[el].errorMessage.notPhone,
            regex.test(tabEl[el].value)
          );
        } else {
          setErrorMessages(tabEl[el].errorMessage.notPhone, true);
        }
      }

      if (tabEl[el].validationRules.isEqual) {
        if (tabEl[el].value !== '') {
          isValid = isValid && tabEl[el].value === form.password.value;
          setErrorMessages(
            tabEl[el].errorMessage.notEqual,
            tabEl[el].value === form.password.value
          );
        }
      }
    }

    setFormIsValid(isValid);
  };

  const checkStep = (step) => {
    switch (step) {
      case 1:
        checkValidity([form.firstname, form.lastname]);
        break;
      case 2:
        checkValidity([form.email, form.password, form.confirmedPassword]);
        break;
      case 3:
        checkValidity([form.phone]);
        break;
      case 4:
        console.log('4');
        checkValidity([form.profile]);
        break;
      default:
        break;
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    checkStep(step);
  };

  const submitForm = () => {
    console.log('form submitted');
    let data = {};
    console.log(form);
    for (const el in form) {
      console.log(form[el]);
      data[el] = form[el].value;
    }

    console.log(data.profile, 'data');

    makeCompressor(data.profile, {
      quality: 0.6,
      width: 500,
      success(result) {
        const formData = new FormData();

        formData.append('file', result, result.name);
        formData.append('upload_preset', 'h19ajqlm');

        formData.append('folder', 'profile_picture');

        axios
          .post(
            'https://api.cloudinary.com/v1_1/theo-cesi/image/upload',
            formData
          )
          .then((res) => {
            data.picture = res.data.url;

            axios
              .post(`${process.env.API_ENDPOINT}/auth/signup`, data)
              .then((res) => {
                console.log(res);
                if (res.status === 201) {
                  router.push('/verifiedEmail');
                } else {
                  throw err;
                }
              });
          });
      },
      error(err) {
        setErrors(err.message);
      },
    });
  };

  const _next = () => {
    let currentStep = step;
    currentStep = currentStep >= 3 ? 4 : currentStep + 1;
    setStep(currentStep);
    switch (currentStep) {
      case 2:
        setName(form.firstname.value);
        break;
      case 3:
        setEmail(form.email.value);
        break;
      case 4:
        setPhone(form.phone.value);
        break;
      default:
        break;
    }
    setFormIsValid(false);
  };

  const _prev = () => {
    let currentStep = step;
    currentStep = currentStep <= 1 ? 1 : currentStep - 1;
    setStep(currentStep);
    setFormIsValid(false);
  };

  const formElementsArray = [];
  for (let key in form) {
    formElementsArray.push({
      id: key,
      config: form[key],
    });
  }

  return (
    <>
      {step >= 2 ? <BackArrow onClick={_prev} /> : <BackArrow />}
      <Step1
        currentStep={step}
        formElementsArray={formElementsArray}
        changed={inputChangedHandler}
        changed={inputChangedHandler}
        handleSubmit={handleSubmit}
      />
      <Step2
        currentStep={step}
        formElementsArray={formElementsArray}
        changed={inputChangedHandler}
        changed={inputChangedHandler}
        handleSubmit={handleSubmit}
      />
      <Step3
        currentStep={step}
        formElementsArray={formElementsArray}
        changed={inputChangedHandler}
        changed={inputChangedHandler}
        handleSubmit={handleSubmit}
      />
      <Step4
        currentStep={step}
        formElementsArray={formElementsArray}
        changed={inputChangedHandler}
        changed={inputChangedHandler}
        handleSubmit={handleSubmit}
        name={name}
        email={email}
        phone={phone}
      />
    </>
  );
};

export default StepForm;

function Step1({ currentStep, formElementsArray, changed, handleSubmit }) {
  if (currentStep !== 1) {
    return null;
  }
  return (
    <form className='mt-5' onSubmit={handleSubmit}>
      <div className='form-group m-0'>
        {formElementsArray.map(
          (formElement) =>
            formElement.config.step === 1 && (
              <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                valid={formElement.config.valid}
                required={formElement.config.required}
                onChange={(event) => changed(event, formElement.id)}
                className={formElement.config.className}
              />
            )
        )}
        <div className='row justify-content-center'>
          <Button type='submit' className='white btn-submit py-2 px-4 mt-3'>
            Suivant
          </Button>
        </div>
      </div>
    </form>
  );
}

function Step2({ currentStep, formElementsArray, changed, handleSubmit }) {
  if (currentStep !== 2) {
    return null;
  }
  return (
    <form className='my-5 pb-5' onSubmit={handleSubmit}>
      <div className='form-group m-0'>
        {formElementsArray.map(
          (formElement) =>
            formElement.config.step === 2 && (
              <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                valid={formElement.config.valid}
                required={formElement.config.required}
                onChange={(event) => changed(event, formElement.id)}
                className={formElement.config.className}
              />
            )
        )}
        <div className='row justify-content-center mb-4'>
          <Button type='submit' className='white btn-submit py-2 px-4 mt-3'>
            Suivant
          </Button>
        </div>
      </div>
    </form>
  );
}

function Step3({ currentStep, formElementsArray, changed, handleSubmit }) {
  if (currentStep !== 3) {
    return null;
  }
  return (
    <form className='my-5 pb-5' onSubmit={handleSubmit}>
      <div className='form-group m-0'>
        {formElementsArray.map(
          (formElement) =>
            formElement.config.step === 3 && (
              <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                valid={formElement.config.valid}
                required={formElement.config.required}
                onChange={(event) => changed(event, formElement.id)}
                className={formElement.config.className}
              />
            )
        )}
        <div className='row justify-content-center mb-5 pt-5'>
          <Button type='submit' className='white btn-submit py-2 px-4 mt-3'>
            Suivant
          </Button>
        </div>
      </div>
    </form>
  );
}

function Step4({
  currentStep,
  formElementsArray,
  changed,
  handleSubmit,
  name,
  email,
  phone,
}) {
  if (currentStep !== 4) {
    return null;
  }
  return (
    <form className='my-5 pb-5' onSubmit={handleSubmit}>
      <div className='form-group m-0'>
        {formElementsArray.map(
          (formElement) =>
            formElement.config.step === 4 && (
              <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                valid={formElement.config.valid}
                required={formElement.config.required}
                onChange={(event) => changed(event, formElement.id)}
                className={formElement.config.className}
              />
            )
        )}
        <h3 className='text-center'>{name}</h3>
        <p className='text-center white-50'>{email}</p>
        <p className='text-center white-50'>{phone}</p>
        <div className='row justify-content-center pt-5'>
          <Button type='submit' className='white btn-submit py-2 px-4 mt-3'>
            Valider
          </Button>
        </div>
      </div>
    </form>
  );
}
