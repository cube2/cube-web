import React from 'react';

const HeadTitle = ({ title, subtitle }) => {
  return (
    <div className='d-flex flex-column my-5'>
      <h2 className='m-0 font-weight-bold'>{title}</h2>
      <p className='white-50 m-0'>{subtitle}</p>
    </div>
  );
};

export default HeadTitle;
