import React from 'react';

//Components
import LogoButton from '../../components/LogoButton/LogoButton';

const SignInWith = () => {
  return (
    <div className='d-flex flex-column align-items-center mt-5'>
      <div className='row align-items-center justify-content-between w-100'>
        <div className='split-separator'></div>
        <p className='white-50 m-0 sm-font-size'>Ou continuer avec</p>
        <div className='split-separator'></div>
      </div>
      <div className='row mt-5 w-50 justify-content-around'>
        <img src={`/img/svg/apple.svg`} alt='' />
        <img src={`/img/svg/google.svg`} alt='' />
        <img src={`/img/svg/github.svg`} alt='' />
      </div>
    </div>
  );
};

export default SignInWith;
