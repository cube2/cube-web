import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import styles from '../styles/Home.module.scss';

//Components
import Button from '../components/Button/Button';

export default function Home() {
  const router = useRouter();

  useEffect(() => {
    if (localStorage.getItem('firstname')) {
      router.replace('/groupe');
    }
  }, []);

  return (
    <div
      className='container d-flex align-items-center justify-content-between'
      style={{ height: '100vh' }}
    >
      <img src='/img/logoAgora.png' alt='' className={'logoAgora'} />

      <div className={`${styles.text} col-lg-6`}>
        <div className='d-lg-none'>
          <img src='/img/logoAgora.png' alt='' />
        </div>
        <h1 className='d-none d-lg-block'>
          Agora <br />
        </h1>
        <p className='px-4 px-lg-0 white-50 mb-lg-5'>
          Avec Agora, profitez d'une communication rapide, simple et qui est
          gratuite, parfait pour partager de bon moments.
        </p>

        <div className={`d-flex ${styles.btnContainer} mt-lg-5 btns`}>
          <Button
            href={'/inscription'}
            className={'btn btn-primary mb-4 mb-lg-0 mr-lg-4 px-5'}
          >
            Inscription
          </Button>

          <Button href={'/connexion'} className={'btn btn-secondary px-5'}>
            Connexion
          </Button>
        </div>
      </div>

      <img src='/img/illu.svg' alt='' className={styles.illu} />
      <div className={`${styles.download} d-none d-lg-block dl`}>
        <img src='img/svg/google-play-badge.svg' alt='' />
        <img src='img/svg/app-store-badge.svg' alt='' />
      </div>
    </div>
  );
}
