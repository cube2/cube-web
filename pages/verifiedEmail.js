import React from 'react';
import styles from '../styles/VerifiedEmail.module.scss';

const verifiedEmail = () => {
  return (
    <div className={styles.VerifiedEmail}>
      <div className={styles.VerifiedEmail__box}>
        <img src='/img/smallLogo.png' alt='' />
        <p>
          Un mail de vérification de compte vient de vous être envoyé. <br />
          Veuillez regarder votre boite mail.
        </p>
      </div>
    </div>
  );
};

export default verifiedEmail;
